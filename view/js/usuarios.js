$(document).ready(function(){
    listarUsuarios()
})

function listarUsuarios() {
    $.ajax({
        url: '../controllers/usuarios.php',
        type: 'POST',
        dataType: 'json',
        data: {opcn:'listarUsuarios'},
    })
    .done(function(res) {
        let tr = '';
        res.forEach((el,index) => {
            console.log(el.nombre)
            tr += `<tr>
                    <td>${el.nombre}</td>
                    <td>${el.apellidos}</td>
                    <td>${el.telefono}</td>
                    <td>${el.email}</td>
                    <td>${el.direccion}</td>
                    <td>${el.estado}</td>
                    <td>${el.fecha_creacion}</td>
                    <td>${el.apellidos}</td>
                </tr>`
        });
        $('#tblusuarios tbody').html(tr)

    })
    .fail(function() {
        console.log("error");
    })
}